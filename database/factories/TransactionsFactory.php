<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Transactions;
use Faker\Generator as Faker;

$factory->define(Transactions::class, function (Faker $faker) {

    return [
        'loketId' => $faker->randomDigitNotNull,
        'userId' => $faker->randomDigitNotNull,
        'number' => $faker->word,
        'date' => $faker->date('Y-m-d H:i:s'),
        'type' => $faker->randomDigitNotNull,
        'status' => $faker->randomDigitNotNull,
        'description' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
