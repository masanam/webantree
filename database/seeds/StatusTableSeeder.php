<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=array(
            array(
                'title'=>'Active',
                'description'=>''
            ),
            array(
                'title'=>'Non-Active',
                'description'=>''
            ),
        );

        DB::table('statuses')->insert($data);
    }
}
