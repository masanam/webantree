<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Admin\Transactions;

class TransactionsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_transactions()
    {
        $transactions = factory(Transactions::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/admin/transactions', $transactions
        );

        $this->assertApiResponse($transactions);
    }

    /**
     * @test
     */
    public function test_read_transactions()
    {
        $transactions = factory(Transactions::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/admin/transactions/'.$transactions->id
        );

        $this->assertApiResponse($transactions->toArray());
    }

    /**
     * @test
     */
    public function test_update_transactions()
    {
        $transactions = factory(Transactions::class)->create();
        $editedTransactions = factory(Transactions::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/admin/transactions/'.$transactions->id,
            $editedTransactions
        );

        $this->assertApiResponse($editedTransactions);
    }

    /**
     * @test
     */
    public function test_delete_transactions()
    {
        $transactions = factory(Transactions::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/admin/transactions/'.$transactions->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/admin/transactions/'.$transactions->id
        );

        $this->response->assertStatus(404);
    }
}
