@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Transactions
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($transactions, ['route' => ['admin.transactions.update', $transactions->id], 'method' => 'patch']) !!}

                        @include('admin.transactions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection