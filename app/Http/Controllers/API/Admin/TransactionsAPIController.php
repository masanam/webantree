<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateTransactionsAPIRequest;
use App\Http\Requests\API\Admin\UpdateTransactionsAPIRequest;
use App\Models\Admin\Transactions;
use App\Repositories\Admin\TransactionsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TransactionsController
 * @package App\Http\Controllers\API\Admin
 */

class TransactionsAPIController extends AppBaseController
{
    /** @var  TransactionsRepository */
    private $transactionsRepository;

    public function __construct(TransactionsRepository $transactionsRepo)
    {
        $this->transactionsRepository = $transactionsRepo;
    }

    /**
     * Display a listing of the Transactions.
     * GET|HEAD /transactions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $transactions = $this->transactionsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($transactions->toArray(), 'Transactions retrieved successfully');
    }

    /**
     * Store a newly created Transactions in storage.
     * POST /transactions
     *
     * @param CreateTransactionsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTransactionsAPIRequest $request)
    {
        $loketId = $request->loketId;
        $tanggal = date('Y-m-d H:i:s');
        $tanggalNow = date('Y-m-d');
        $status = 4;
        $type = 1;

        $total = \App\Models\Admin\Transactions::where('loketId',$loketId)
        ->whereDate('tanggal',$tanggalNow)
        ->count();

        $trans = \App\Models\Admin\Transactions::with('user')->with('loket')->with('stat')
        ->where('userid',$request->userId)
        ->where('loketId',$loketId)
        ->where('status','4')
        ->get();

        if ( count($trans) > 0 ) {
            return $this->sendError('You Already Have a Number');
        }

        $urut = $total + 1;
        $nourut = $loketId.'-'.sprintf('%04d', $urut);

        $request->request->add([
            'number' => $nourut,
            'tanggal' => $tanggal,
            'status' => $status,
            'type' => $type
        ]);

        $input = $request->all();

        $transactions = $this->transactionsRepository->create($input);

        return $this->sendResponse($transactions->toArray(), 'Transactions saved successfully');
    }

    /**
     * Display the specified Transactions.
     * GET|HEAD /transactions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Transactions $transactions */
        $transactions = $this->transactionsRepository->find($id);

        if (empty($transactions)) {
            return $this->sendError('Transactions not found');
        }

        return $this->sendResponse($transactions->toArray(), 'Transactions retrieved successfully');
    }

    /**
     * Update the specified Transactions in storage.
     * PUT/PATCH /transactions/{id}
     *
     * @param int $id
     * @param UpdateTransactionsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransactionsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Transactions $transactions */
        $transactions = $this->transactionsRepository->find($id);

        if (empty($transactions)) {
            return $this->sendError('Transactions not found');
        }

        $transactions = $this->transactionsRepository->update($input, $id);

        return $this->sendResponse($transactions->toArray(), 'Transactions updated successfully');
    }

    /**
     * Remove the specified Transactions from storage.
     * DELETE /transactions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Transactions $transactions */
        $transactions = $this->transactionsRepository->find($id);

        if (empty($transactions)) {
            return $this->sendError('Transactions not found');
        }

        $transactions->delete();

        return $this->sendSuccess('Transactions deleted successfully');
    }

    public function getLoketTransaction(Request $request)
    {
        $loketId = $request->loketId;
        $trans = \App\Models\Admin\Transactions::with('user')->with('loket')->with('stat')
        ->where('loketId',$loketId)
        ->where('status','4')
        ->orderby('number','asc')
        ->get();

        if (empty($trans)) {
            return $this->sendError('Transaction not found');
        }

        foreach ($trans as $row) {     
            $row->hostname = $row->loket->host->title;
            $row->categoryname = $row->loket->host->category->title;
            $row->address = $row->loket->host->address;
            $row->city = $row->loket->host->city;
            $row->phone = $row->loket->host->phone;      
            $row->loketname = $row->loket->title;
            $row->loketimage = $row->loket->image;
            $row->username = $row->user->name;
            $row->statusname = $row->stat->title; 
        }

        return $this->sendResponse($trans->toArray(), 'Users retrieved successfully');
    }

    public function getUserTransaction(Request $request)
    {
        $userId = $request->userId;
        $transList = \App\Models\Admin\Transactions::with('user')->with('loket')->with('stat')
        ->where('userId',$userId)
        ->where('status','4')
        ->orderby('id','desc')
        ->get();

        if (empty($transList)) {
            return $this->sendError('Transaction not found');
        }

        foreach ($transList as $row) {     
            $row->hostname = $row->loket->host->title;
            $row->categoryname = $row->loket->host->category->title;
            $row->address = $row->loket->host->address;
            $row->city = $row->loket->host->city;
            $row->phone = $row->loket->host->phone;      
            $row->loketname = $row->loket->title;
            $row->loketimage = $row->loket->image;
            $row->username = $row->user->name;
            $row->statusname = $row->stat->title;
        }

        // $transList[] =  $host;

        return $this->sendResponse($transList->toArray(), 'Users retrieved successfully');
    }

    public function getUserHistory(Request $request)
    {
        $userId = $request->userId;
        $htrans = \App\Models\Admin\Transactions::with('user')->with('loket')->with('stat')
        ->where('userId',$userId)
        ->orderby('id','desc')
        ->get();

        if (empty($htrans)) {
            return $this->sendError('Transaction not found');
        }

        foreach ($htrans as $row) {     
            $row->hostname = $row->loket->host->title;
            $row->categoryname = $row->loket->host->category->title;
            $row->address = $row->loket->host->address;
            $row->city = $row->loket->host->city;
            $row->phone = $row->loket->host->phone;      
            $row->loketname = $row->loket->title;
            $row->loketimage = $row->loket->image;
            $row->username = $row->user->name;
            $row->statusname = $row->stat->title;
        }

        return $this->sendResponse($htrans->toArray(), 'Users retrieved successfully');
    }

    public function updateTransaction($id, Request $request)
    {
        $transactions = \App\Models\Admin\Transactions::where('id',$id)->first();

        if (empty($transactions)) {
            return $this->sendError('Transaction not found');
        }


      $input = array_merge($request->all(), 
        [
            'status' => 7,
        ]);

        $transactions = \App\Models\Admin\Transactions::where('id',$id)->update($input);
        $transactions = \App\Models\Admin\Transactions::where('id',$id)->get();


        return $this->sendResponse($transactions->toArray(), 'Transaction updated successfully');
    }


}
