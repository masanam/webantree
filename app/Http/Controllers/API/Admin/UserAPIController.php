<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateUserAPIRequest;
use App\Http\Requests\API\Admin\UpdateUserAPIRequest;
use Illuminate\Support\Facades\Storage;
use App\Models\Admin\User;
use App\Repositories\Admin\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class UserController
 * @package App\Http\Controllers\API\Admin
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUserAPIRequest $request)
    {
        $input = $request->all();

        $user = $this->userRepository->create($input);

        return $this->sendResponse($user->toArray(), 'User saved successfully');
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        return $this->sendResponse($user->toArray(), 'User retrieved successfully');
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'User updated successfully');
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendSuccess('User deleted successfully');
    }

    public function getUserProfile(Request $request)
    {
        $email = $request->email;
        $user = \App\Models\Admin\User::where('email',$email)->get();

        if (empty($user)) {
            return $this->sendError('Hosts not found');
        }

        return $this->sendResponse($user->toArray(), 'Users retrieved successfully');
    }

    public function updateProfile($email, Request $request)
    {

        /** @var User $user */
        $user = \App\Models\Admin\User::where('email',$email)->first();

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        // $photo = fopen($request->photo,"r");
        // $extension = $request->photo->extension();
        // $photopath = '';
  
        // if ($extension == 'png' || $extension == 'jpg' || $extension == 'jpeg'){
        //   $file_name = 'photo';
        //   $tgl=date('YmdHis');
        //   $photoname = $file_name.'-'.$tgl.'.jpg';
  
        //   $folder = '/storage/files/shares/photo/';
        //   $photopath = $folder.$photoname;

        // $request->photo->move(public_path($folder),$photoname);
        // $request->photo->store('photos');

        // $url = $request->photo;
        //         if (substr($url,0,4) != 'http') {
        //           $url = 'http://'.$url;
        //         }
        //   }
  
        $photopath = '/storage/files/shares/photo/test.jpg';
        $input = array_merge($request->all(), 
        [
            'photo' => $photopath,
        ]);

        $user = \App\Models\Admin\User::where('email',$email)->update($input);
        $user = \App\Models\Admin\User::where('email',$email)->get();

        return $this->sendResponse($user->toArray(), 'User updated successfully');
    }

}
