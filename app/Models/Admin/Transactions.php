<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Transactions
 * @package App\Models\Admin
 * @version September 15, 2020, 4:16 am UTC
 *
 * @property integer $loketId
 * @property integer $userId
 * @property string $number
 * @property string|\Carbon\Carbon $date
 * @property integer $type
 * @property integer $status
 * @property string $description
 */
class Transactions extends Model
{
    use SoftDeletes;

    public $table = 'transactions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'loketId',
        'userId',
        'number',
        'tanggal',
        'type',
        'status',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'loketId' => 'integer',
        'userId' => 'integer',
        'number' => 'string',
        'tanggal' => 'datetime',
        'type' => 'integer',
        'status' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'loketId' => 'required|integer',
        'userId' => 'required|integer',
        'number' => 'nullable|string|max:191',
        'tanggal' => 'nullable',
        'type' => 'nullable|integer',
        'status' => 'nullable|integer',
        'description' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function stat()
    {
        return $this->belongsTo(\App\Models\Admin\Statuses::class, 'status', 'id');
    }
    
    public function user()
    {
        return $this->belongsTo(\App\Models\Admin\User::class, 'userId', 'id');
    }

    public function loket()
    {
        return $this->belongsTo(\App\Models\Admin\Lokets::class, 'loketId', 'id');
    }



}
